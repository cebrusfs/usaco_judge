#include<assert.h>
#include<cstdio>
#include<cstdlib>
#include<string>
#define sMax 100005

using namespace std;

char str[sMax];

int main()
{
    system("ls code/*.cpp > ls2");

    FILE *p = fopen("ls2", "r");
    assert(p != NULL);

    system("rm result.txt > /dev/null 2> /dev/null");
    int s1 = 0, s2 = 0;
    int pn = 0;
    while(fgets(str, sMax, p))
    {
        pn ++ ;
        str[strlen(str)-1] = 0;

        string ts = "./main ";
        ts += str;
        ts += " -judge >> result.txt";


        printf("Juding %s ... ", str);
        fflush(stdout);
        int t = system(ts.c_str());

        if(!t)
        {
            printf("Error\n");
            fflush(stdout);
            continue;
        }
        t /= 256;
        int v = 1;
        int a = 0, b = 0;
        for(int i=1;i<=22;i++)
            for(int j=0;j<=i;j++)
            {
                if(v == t)
                    a = j, b = i;
                    
                v++;
            }

        s1 += 20.0 * a / b;
        s2 += 100.0 * a / b;
        printf("\t%d / %d\n", a, b);
        fflush(stdout);
    }

    p = fopen("result.txt", "a");
    fprintf(p, "Total: %d / %d,  %d / %d\n", s1, 20*pn, s2, 100*pn);

    system("rm ls2 > /dev/null 2> /dev/null");

    printf("\nDone. %d / %d,  %d / %d\n", s1, 20*pn, s2, 100*pn);
}
