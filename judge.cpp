#include<iostream>
#include<string>
#include<sys/time.h>
#include<sys/resource.h>
#include<cstdio>
#include<cstdlib>
#include<cstring>

using namespace std;

string checkname(string str)
{
    string s;
    for(int i=str.size()-1;i>=0;i--)
    {
        if(str[i] != '/')
            s += str[i];
        else
            break;
    }
    for(int i=0;i+i<(int)s.size();i++)
        swap(s[i], s[s.size()-i-1]);

    return s;
}

int main(int argc, char* argv[])
{
	if(argc != 6)
	{
		printf("judge: usage: judge SolutionFile InputFile OutputFile TimeLimit(s) MemoryLimit(MB)\n");
		exit(1);
	}
	string Execution = argv[1], Input_File = argv[2], Output_File = argv[3];
    long long TimeLimit, MemoryLimit;
    sscanf(argv[4], "%lld", &TimeLimit);
    sscanf(argv[5], "%lld", &MemoryLimit);
    MemoryLimit *= 1024 * 1024;

    if(fork()==0)
    {
        rlimit Time;

        //Case Time Limit: (s)
        Time.rlim_cur = TimeLimit;
        Time.rlim_max = TimeLimit;

        //Execution
        string Command = Execution;
        Command += " < " + Input_File;
        Command += " > output 2>/dev/null";

        setrlimit(RLIMIT_CPU, &Time);
        int status = system(Command.c_str());
        return status/256;
    }
    int status;
    rusage usage;
    wait3(&status, 0, &usage);

    long long exectime = (usage.ru_utime.tv_sec + usage.ru_stime.tv_sec) * 1000 + 
                      (usage.ru_utime.tv_usec + usage.ru_stime.tv_usec) / 1000;

    long long memoryused = usage.ru_maxrss;

    string Name = checkname(Input_File);
    int num;
    sscanf(Name.c_str(), "%*[^.].%d", &num);
    printf("%-3d: ",  num);

    int result = 1;
    if(exectime >= TimeLimit*1000)
        printf("%-21s", "Time Limit Exceeded");
    else if(memoryused >= MemoryLimit)
        printf("%-21s", "Memory Limit Exceeded");
    else if(status != 0)
        printf("%-21s", "Runtime Error");
    else
    {
        string Command = "diff -Bwq output ";
        Command += Output_File + " >/dev/null 2>/dev/null";

        //cout << Output_File << "\n";
		
        result = system(Command.c_str());
		if(!result)
			printf("%-21s", "Accepted");
		else
			printf("%-21s", "Wrong Answer"), result=1;
    }
    printf(" ( %4.1lld ms, %5.2f MB)\n", exectime, memoryused / 1024.0 / 1024);
    system("rm -rf output");
    return result;
}
