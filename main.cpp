#include<iostream>
#include<string>
#include<vector>
#include<sys/time.h>
#include<cstdio>
#include<cstdlib>
#include<cstring>

#define Max 30000

using namespace std;

char tmp[Max];

string checkname(char str[])
{
    if( strstr(str, ".cpp") )
    {
        int l = strlen(str);

        if(l >= 4)
            str[l-4] = 0;
    }

    string s;
    int l = strlen(str);
    for(int i=l-1;i>=0;i--)
    {
        if(str[i] != '/')
            s += str[i];
        else
            break;
    }
    for(int i=0;i+i<(int)s.size();i++)
        swap(s[i], s[s.size()-i-1]);

    if(str[0] == '.')
    {
        l = strlen(str);
        for(int i=0;i<l;i++)
            str[i] = str[i+2];
    }


    return s;
}

bool USACO_new = 0;
long long Time, Memory;
vector<string> arg;
bool InJudge = 0;

void setarg()
{
    Time = 1, Memory = 16;
    for(int i=0;i<(int)arg.size();i++)
    {
        if(arg[i][0] == '-' && arg[i][1] == 'n')
            Time = 2, Memory = 64;
        if(arg[i][0] == '-' && arg[i][1] == 'j')
            InJudge = 1;
    }

    for(int i=0;i<(int)arg.size();i++)
    {
        if(arg[i][2] == '=')
        {
            long long tn;
            char tc;
            sscanf(arg[i].c_str(), "-%c=%lld", &tc, &tn);

            if(tc == 't')
                Time = tn;
            else if(tc == 'm')
                Memory = tn;
        }
    }
}

int main(int argc, char* argv[])
{
	if(argc < 2 || argc >= 5)
	{
		printf("main : usage: main Solution_File [t=TimeLimit] [m=MemoryLimit]\n");
		return 0;
	}
    for(int i=2;i<argc;i++)
        arg.push_back(argv[i]);
    setarg();

    string Name = checkname(argv[1]);
    printf("PROB: %s\n", Name.c_str());
    printf("Time Limit: %lld s, Memory Limit: %lld MB\n", Time, Memory);
    printf("Compiling ... ");
	sprintf(tmp, "g++ %s.cpp -O2 -o %s 1>/dev/null 2>/dev/null", argv[1], argv[1]);
    int compile_result = system(tmp);
    if(compile_result)
    {
        printf("Error.\n");
        return 0;
    }
    else
        printf("OK.\n");
    printf("\n");

    fflush(stdout);

	sprintf(tmp, "ls ./testdata/%s.*.in > ls.txt 2>/dev/null", Name.c_str());
    system(tmp);

	FILE *pFile = fopen("ls.txt", "r");

	int AC = 0, Case = 0;
	for(int i=0;fscanf(pFile, "%s", tmp)==1;i++)
        Case++;

    for(int i=1;i<=Case;i++)
    {
        string Input, Output;

        sprintf(tmp, "./testdata/%s.%d.in", Name.c_str(), i);

        Output = Input = tmp;

		int idx = Output.find(".in");
        Output.replace(idx, 3, ".out");


        string Command = "./judge ";
        Command += " ./";
        Command += argv[1];
        Command += " " + Input;
        Command += " " + Output;
        Command += " ";
        sprintf(tmp, "%lld", Time);
        Command += tmp;
        Command += " ";
        sprintf(tmp, "%lld", Memory);
        Command += tmp;
        if(InJudge)
            Command += " >> result.txt";
        Command += " 2>/dev/null";

        int result = system(Command.c_str());
        
        if(!result)
            AC++;
	}
	system("rm -rf ls.txt");
    sprintf(tmp, "rm -rf %s", argv[1]);
    system(tmp);

    if(!InJudge)
	    printf("Result: %d / %d\n", AC, Case);
    else
	    printf("Score: %.0lf / 20, %.0lf / 100\n", 20.0*AC/Case, 100.0*AC/Case);
    printf("\n\n");

    int v = 1;
    for(int i=1;i<=22;i++)
        for(int j=0;j<=i;j++)
        {
            if(j == AC && i == Case)
                return v;
            v++;
        }
    return 0;
}
